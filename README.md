# Simple package to verify imports from @norsktest.dkuser works

## Installation
```bash
yarn install
```

## alternate installation
You still have to run `yarn install``, but afterwards you can use yarn link to link the package to the dkuimport package.  This way you can make changes to this package and see them reflected in the dkuimport package without having to run yarn install every time.
```bash
dkuser> yarn link
dkuser> cd ../../dkuimport (this package)
dkuimport> yarn link @norsktest.dkuser
```



## Usage (run)
```bash
yarn dev
```
This runs the command listed in package.json under `scripts.dev`.

## notes

In package.json
   
    "type": "module", (makes it so you can use import instead of require)

